<?php

namespace Drupal\itcross_devel\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Class DefaultController.
 */
class DefaultController extends ControllerBase {

  /**
   * Test_page.
   *
   * @return array
   *   Return some result
   */
  public function test_page() {
    return [
      '#type' => 'markup',
      '#markup' => $this->t('Implement method: test_page')
    ];
  }

}
