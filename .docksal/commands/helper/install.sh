#!/usr/bin/env bash

#-------------------------- Helper functions --------------------------------

# Copy a settings file.
# Skips if the destination file already exists.
# @param $1 source file
# @param $2 destination file
copy_settings_file()
{
	local source="$1"
	local dest="$2"

	if [[ ! -f $dest ]]; then
    echo "Copying $filename from ${source} to ${dest}..."
		cp $source $dest
	else
		echo "${dest} already in place."
	fi
}

# Change a settings file.
# Remove if the destination file already exists.
# @param $1 source file
# @param $2 destination file
change_settings_file()
{
	local source="$1"
	local dest="$2"
	local filename=$(basename "$2")

	if [[ -f $dest ]]; then
		echo-yellow "Removing existing ${filename} file..."
		rm $dest
	fi
  echo "Copying $filename from ${source} to ${dest}..."
	cp $source $dest
}

# Initialize local settings files
init_settings ()
{
 	local source="$1"
  echo "Initializing local setting file..."
	# Copy from settings templates
	change_settings_file ${source} "${SITEDIR_PATH}/settings.local.php"
}

# Composer Install
composer_install ()
{
  echo-green "Composer Installing ..."
  composer install
}

# Post Install
post_install ()
{
  echo-green "Post install actions ..."
  echo "Running cron ..."
	cd $DOCROOT_PATH
	drush cron
	cd ${PROJECT_ROOT}
}
