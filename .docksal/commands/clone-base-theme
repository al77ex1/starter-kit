#!/bin/bash

## Create new theme.

usage ()
{
  echo "Usage for create new theme: fin `basename $0` <theme name>"
  echo "Examples:"
  echo "Create: fin clone-base-theme mytheme"
  exit 0
}

# Variables
theme_path=${PROJECT_ROOT}/web/themes/custom;

# Help script
source ${PROJECT_ROOT}/.docksal/commands/helper/echo-style.sh

# Grape the theme name argument.
if [ ! -z "$1" ]; then
  arg1="$1";
  if [[ $arg1 =~ ^[A-Za-z][A-Za-z0-9_]*$ ]]; then
    theme_name="$arg1";
  else
    echo-red "Theme name is not a valid theme name!";
    usage
    exit 1;
  fi
else
  echo-red "Please add the name of your theme!";
  usage
  exit 1;
fi

# Create the new theme if we do not have a folder with that name yet.
if [[ ! -d "$theme_path/$theme_name" ]]; then

  # Copy the bootstrap_barrio_itcross folder to your custom theme location.
  cp -r ${theme_path}/bootstrap_barrio_itcross ${theme_path}/${theme_name};

  # Rename bootstrap_barrio_itcross files
  mv ${theme_path}/${theme_name}/bootstrap_barrio_itcross.info.yml ${theme_path}/${theme_name}/${theme_name}.info.yml ;
  mv ${theme_path}/${theme_name}/bootstrap_barrio_itcross.libraries.yml ${theme_path}/${theme_name}/${theme_name}.libraries.yml ;
  mv ${theme_path}/${theme_name}/bootstrap_barrio_itcross.theme ${theme_path}/${theme_name}/${theme_name}.theme;
  mv ${theme_path}/${theme_name}/bootstrap_barrio_itcross.breakpoints.yml ${theme_path}/${theme_name}/${theme_name}.breakpoints.yml;
  mv ${theme_path}/${theme_name}/config/install/bootstrap_barrio_itcross.settings.yml ${theme_path}/${theme_name}/config/install/${theme_name}.settings.yml;
  mv ${theme_path}/${theme_name}/config/schema/bootstrap_barrio_itcross.schema.yml ${theme_path}/${theme_name}/config/schema/${theme_name}.schema.yml;

  # Replace all bootstrap_barrio_itcross with the machine name of your theme.
  grep -rl 'bootstrap_barrio_itcross' ${theme_path}/${theme_name} | xargs sed -i "s/bootstrap_barrio_itcross/${theme_name}/g";
  grep -rl 'Bootstrap Barrio ITCross' ${theme_path}/${theme_name} | xargs sed -i "s/Bootstrap Barrio ITCross/${theme_name^}/g";

  echo-yellow "The new theme ${theme_name^} has been created at \"${theme_path}/${theme_name}\" ";
  # There should not be exit code because this command is used

else
  echo-red "The folder \"${theme_path}/${theme_name}\" is already in the site!";
  exit 1;
fi
